const tamperMonkeyHeaders = {
    name: 'Galantis extensions',
    namespace: 'https://regapictures-userscripts.gitlab.io/',
    source: 'https://gitlab.com/regapictures-userscripts/galantis-tools',
    version: `[version]`,
    description: 'Extensions for EM-group\'s Galantis',
    author: 'regapictures',
    downloadURL: 'https://regapictures-userscripts.gitlab.io/galantis-tools/bundle.user.js',
    updateURL: 'https://regapictures-userscripts.gitlab.io/galantis-tools/bundle.meta.js',
    include: 'https://emgroup.emcloud.io/*'/* ,
    grant: ['GM_getValue', 'GM_setValue', 'GM_deleteValue'] */
}

const path = require('path');
const merge = require('webpack-merge');
const WebpackUserscript = require('webpack-userscript')
const dev = process.env.NODE_ENV === 'development'

const finalTamperMonkeyHeaders = dev ?
    merge(
        tamperMonkeyHeaders, {
            name: 'Local Development',
            namespace: 'http://localhost:8080/',
            version: `[version]-build.${Math.floor((new Date()).getTime() / 1000)}.[buildNo]`,
            description: 'Development plugin. The contents of this plugin will change as you switch between projects.',
            author: 'local-dev',
            downloadURL: 'http://localhost:8080/bundle.user.js',
            updateURL: 'http://localhost:8080/bundle.meta.js'
        }
    ) : tamperMonkeyHeaders;

module.exports = {
    mode: dev ? 'development' : 'production',
    entry: {
        'bundle': path.resolve(__dirname, 'src', 'index.ts')
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    optimization: {
        minimize: false
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist')
    },
    plugins: [
        new WebpackUserscript({ headers: finalTamperMonkeyHeaders })
    ]
};