# Galantis tools
Unofficial userscript for Galantis Web Platform (EM-Group).

## Install
https://regapictures-userscripts.gitlab.io/galantis-tools/bundle.user.js

## Download
https://gitlab.com/regapictures-userscripts/galantis-tools/-/jobs/artifacts/master/raw/dist/bundle.user.js?job=build