import { Registrations } from "./models/Registrations";
import { Time } from "./models/Time";

module.require('./extensions/Number');

const ElementClass = "galantis-tools-element";
const StopTimeRowClass = "stop-time";

let calendarClickListener: (() => any) | undefined;
let canceller: () => void = () => undefined;

function calculateStopTime(toPerformDuration: Time, registrationTimes: string[]): Time | undefined {
    return Registrations
        .fromStrings(registrationTimes)
        .getNeutralStopTimeFor(toPerformDuration);
}

function getRegistrationTimes(registrationTableSection: HTMLTableSectionElement): string[] {
    const times: string[] = [];
    const timeStrings = registrationTableSection.querySelectorAll('div.ng-star-inserted:not(.' + ElementClass + ') > registration:nth-child(1) > div:nth-child(1) > div:nth-child(2)');
    for (let i = 0; i < timeStrings.length; i++) {
        times.push(timeStrings[i].innerHTML);
    }

    return times;
}

function getNgContentAttributeOfRegistrationComponent() {
    const registrationsTableElement = document.querySelector('registration .registrationsTable');
    const matchingAttributes = (registrationsTableElement?.getAttributeNames() ?? [])
        .filter(attr => attr.startsWith('_ngcontent'));
    return matchingAttributes.length === 0 ? "" : matchingAttributes[0];
}

function getToPerformTime(): Time | undefined {
    const toPerformElement = findToPerformElement();
    if (toPerformElement) {
        return Time.parseHHMMInString(toPerformElement.innerText);
    } else {
        return undefined;
    }
}

function createStopTimeTableRow(stopTime: Time) {
    const element = document.createElement('div');
    element.style.fontStyle = 'italic';
    element.className = ElementClass + ' ' + StopTimeRowClass + ' registrationsTable';
    const requiredNgContentAttribute = getNgContentAttributeOfRegistrationComponent();
    element.setAttribute(requiredNgContentAttribute, "");

    createStopTimeTableRowCells(stopTime).forEach(child => {
        child.setAttribute(requiredNgContentAttribute, "");
        element.appendChild(child);
    });

    return element;
}

function createStopTimeTableRowCells(stopTime: Time) {
    const timeCell = document.createElement('div');
    timeCell.innerText = stopTime.toString();

    const descriptionCell = document.createElement('div');
    descriptionCell.innerText = 'Neutrale stoptijd';

    return [
        document.createElement('div'),
        document.createElement('div'),
        timeCell,
        descriptionCell,
        document.createElement('div'),
        document.createElement('div')
    ];
}

function removeStopTimeTableRow() {
    const row = document.querySelector('.' + ElementClass + '.' + StopTimeRowClass);
    if (row != undefined) {
        row.remove();
    }
}

function findRegistrationRowsContainer(): HTMLElement | undefined {
    return <HTMLElement | undefined>document.querySelector('app-dashboard-registrations div:not(first-child) mat-card.table-card > mat-card-content mat-card.table-card > mat-card-content');
}

function findToPerformElement() {
    return <HTMLElement | undefined>document.querySelector('app-dashboard-registrations div:not(first-child) mat-card.table-card div.base-wrapper mat-icon.fa.fa-clock-o + span.marginRight-20:nth-child(2)');
}

function waitForConditionToBeTrue(condition: () => boolean, timeoutMs: number, onLoadComplete: () => void, totalAttemptTimeMs: number = 0): () => void {
    let cancel: () => void = () => undefined;
    if (condition()) {
        onLoadComplete();
    } else if (totalAttemptTimeMs <= timeoutMs) {
        const timeout = setTimeout(() => {
            cancel = waitForConditionToBeTrue(condition, timeoutMs, onLoadComplete, totalAttemptTimeMs + 500);
        }, 500);
        cancel = () => clearTimeout(timeout);
        return () => cancel();
    }

    return cancel;
}

function calculateNeutralStopTime() {
    removeStopTimeTableRow();
    canceller();
    canceller = waitForConditionToBeTrue(
        () => findRegistrationRowsContainer() != undefined,
        1000,
        () => {
            const registrationTableBody = <HTMLTableSectionElement>findRegistrationRowsContainer();
            const times = getRegistrationTimes(registrationTableBody);
            const toPerformDuration = getToPerformTime();
            const stopTime = toPerformDuration == null ? undefined : calculateStopTime(toPerformDuration, times);
            if (stopTime) {
                registrationTableBody.appendChild(createStopTimeTableRow(stopTime));
            }
        }
    );
}

function listenForCalendarClicks() {
    const calendarDaysWrapper = document.querySelector('app-dashboard-calendar > multi-date-picker');
    if (calendarDaysWrapper != undefined) {
        if (calendarClickListener != undefined) {
            calendarDaysWrapper.removeEventListener('click', calendarClickListener);
        }
        const listener = () => setTimeout(() => calculateNeutralStopTime(), 50);
        calendarDaysWrapper.addEventListener('click', listener);
        calendarClickListener = listener;
    }
}

function listenForPageNavigation(onChange: () => void) {
    var pushState = window.history.pushState;
    window.history.pushState = function (state) {
        onChange();
        return pushState.apply(window.history, <any>arguments);
    };
}

function calculateNeutralStopTimeAndListenForDateChanges() {
    calculateNeutralStopTime();
    setTimeout(() => listenForCalendarClicks(), 1000);
}

function run() {
    waitForConditionToBeTrue(
        () => document.querySelector('galantis-app mat-sidenav-container router-outlet') != undefined,
        90000,
        () => {
            calculateNeutralStopTimeAndListenForDateChanges();
            listenForPageNavigation(() => calculateNeutralStopTimeAndListenForDateChanges());
        }
    );
}

run();
