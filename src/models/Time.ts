export class Time {
    private static readonly Pattern = /(\d+)[:uh](\d+)/;
    private minutes: number;

    constructor(minutes: number = 0) {
        this.minutes = minutes;
    }

    public static from(hours: number, minutes: number) {
        return new Time(hours * 60 + minutes);
    }

    public static now() {
        const date = new Date();
        return Time.from(date.getHours() * 60, date.getMinutes());
    }

    public static parseHHMMInString(text: string) {
        const time = Time.attemptParseHHMMInString(text);
        if (time == undefined) {
            throw new Error('[galantis-tools] text does not contain HH:MM: ' + text);
        } else {
            return time;
        }
    }

    public static attemptParseHHMMInString(text: string): Time | undefined {
        const matches = text.match(this.Pattern);
        if (matches == undefined || matches.length < 3) {
            return undefined;
        }
        
        const hours = Number(matches[1]);
        const minutes = Number(matches[2]);
        return new Time(hours * 60 + minutes);
    }

    public plus(delta: Time) {
        return new Time(this.minutes + delta.minutes);
    }

    public minus(delta: Time) {
        return new Time(this.minutes - delta.minutes);
    }

    public toString(seperator: string = ':') {
        const hours = Math.floor(this.minutes / 60);
        const minutes = this.minutes - hours * 60;
        return hours + seperator + minutes.zeroPrefix();
    }
}